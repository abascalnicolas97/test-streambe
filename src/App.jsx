import Rutas from './routes/rutas'
import { AppTheme } from './theme/AppTheme'
import { Provider } from 'react-redux'
import { store } from './store/store'

function App() {
 

  return (
    <Provider store={store}>
    <AppTheme>
     <Rutas/>
     </AppTheme>
     </Provider>
  )
}

export default App
