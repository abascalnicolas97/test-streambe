import { CircularProgress, Grid } from '@mui/material'
import React from 'react'
import './table.css'
import Row from '../row/Row'
import useTable from '../../hook/useTable'

const TableHome = () => {
    const data=useTable()

    if(data.length == 0){
        return (<Grid container display={'flex'} justifyContent={'center'} alignItems={'center'} >
            <CircularProgress color='secondary' sx={{height:50,width:50}}/>
        </Grid>)
    }
    return (

        <Grid container className='table'>
           
            <Grid container className='headerTable' >
                <Grid item xs={5} sm={6} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                    <p className='headerTableTitle'>Name</p>
                </Grid>
                <Grid item xs={5}  display={'flex'} justifyContent={'center'} alignItems={'center'}className='column'>
                    <p className='headerTableTitle'>Birth Date</p>
                </Grid>
                <Grid item xs={2} sm={1} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                    <p className='headerTableTitle'>Photo</p>
                </Grid>
            </Grid>
            {data?.map((userData,index)=> <Row key={index} {...userData }/>)}
        </Grid>
    )
}

export default TableHome
