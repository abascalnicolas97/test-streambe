import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logOut, setUser } from '../store/slices/usuarios';




export const useCheckAuth = () => {
  
    const { usuario } = useSelector( state => state.user );
    
    const dispatch = useDispatch();
    useEffect(() => {
        
        const user = JSON.parse(localStorage.getItem('user'));
        if ( !user ){ 
            dispatch( logOut() );
        }
        else{ 
            dispatch( setUser(user) );
        }
   
    }, []);

    return usuario;
}