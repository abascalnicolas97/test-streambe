import React from 'react'
import { Routes, Route} from 'react-router-dom'
import Login from '../screen/Login/Login'
import Home from '../screen/Home/Home'
import { useCheckAuth } from '../hook/useCheckAuth'

const Rutas = () => {
  const state =useCheckAuth();
 
  return (
    <Routes>
       { !!state ? 
        <Route path='/*' element={<Home/>}/>
      :
        <Route path='/*' element={<Login/>}/>}
    </Routes>
  )
}

export default Rutas
