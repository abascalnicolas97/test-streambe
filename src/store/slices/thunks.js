import axios from 'axios';
import { setUser } from './usuarios';
import { userLogin } from '../../services/services';



export const getUser = (setErrorMessage ) => {
    return async( dispatch, getState ) => {
        try{
            const data =  await userLogin()
            dispatch( setUser({ usuario: data }) );
            localStorage.setItem('user', JSON.stringify(data));
        }
        catch(error){
            setErrorMessage('hubo un error')
        }

    }
}