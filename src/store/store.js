import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./slices/usuarios";

export const store = configureStore({
    reducer:{
            user: userSlice.reducer
    }
}) 